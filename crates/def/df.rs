use std::fmt;
use std::collections::HashMap;

pub struct Problem {
    pub ff: Vec<AnnotatedFolFormula>,
}

pub struct AnnotatedFolFormula {
     pub name: String,
     pub role: FormulaRole,
     pub formula: FolFormula,
}

//--------
// pub enum TptpInput {
//     Annotated(AnnotatedFormula),
//     Incl(Include),
// }
//
// pub enum AnnotatedFormula {
//     Fof(FofAnnotated),
// }
// pub type FofAnnotated = (String, FormulaRole, FolFormula);
// pub type Include = (String, Option<Vec<String>>);


pub enum FormulaRole{
    Axiom,
    Hypothesis,
    Definition,
    Assumption,
    Lemma,
    Theorem,
    Corollary,
    Conjecture,
    Negated_conjecture,
    Plain,
    Type,
    Fi_domain,
    Fi_functors,
    Fi_predicates,
    Unknown,
}

#[derive(Clone)]
pub enum GTerm {
    Variable(String),
    Constant(String),
    Functor(String,Vec<GTerm>),
}

#[derive(Clone)]
pub enum FolFormula {
    Exists(Vec<GTerm>,Box<FolFormula>),
    Forall(Vec<GTerm>,Box<FolFormula>),
    Equiv(Box<FolFormula>,Box<FolFormula>),
    Impl(Box<FolFormula>,Box<FolFormula>),
    Or(Vec<FolFormula>),
    And(Vec<FolFormula>),
    Neg(Box<FolFormula>),
    Atom(Box<GTerm>),
}


pub enum PCFormula{
    A(Vec<Term>,Vec<Term>,Vec<PCFormula>),
    E(Vec<Term>,Vec<Term>,Vec<PCFormula>),
    //Goal(Vec<GTerm>,Vec<GTerm>),
    //ELeaf(Vec<GTerm>,Vec<GTerm>),
}

pub enum Quantifier{
    Forall,
    Exists,
}
#[derive(Clone)]
pub enum Term{
    Variable(usize),
    Constant(usize),
    Functor(usize,Vec<Term>),
}


pub fn compare_terms(t1:&Term, t2:&Term) -> bool{
    match (t1,t2){
        (&Term::Constant(ref x1), &Term::Constant(ref x2)) if x1 == x2 => true,
        (&Term::Variable(ref x1), &Term::Variable(ref x2)) if x1 == x2 => true,
        (&Term::Functor(ref x1, ref args1), &Term::Functor(ref x2, ref args2)) if x1 == x2 => {
            if args1.len() != args2.len(){
                panic!("У одноименных функторов почему-то разное количество аргументов.")
            }
            args1.into_iter()
                .zip(args2.into_iter())
                .map(|(a1,a2)| compare_terms(a1,a2))
                .fold(true,|acc,it| acc & it)
        },
        _ => false
    }
}

//является ли v2 подмножеством v1?
pub fn subset_terms(v1:&Vec<Term>,v2:&Vec<Term>) -> bool{
    v2.into_iter()
        .map(|t2| v1.iter()
                    .any(|t1| compare_terms(t1,t2)))
        .fold(true,|acc,it| acc & it)
}

//=================================================================================================

impl fmt::Display for FormulaRole {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self{
            FormulaRole::Axiom => write!(f,"Axiom"),
            FormulaRole::Hypothesis => write!(f,"Hypothesis"),
            FormulaRole::Definition => write!(f,"Definition"),
            FormulaRole::Assumption => write!(f,"Assumption"),
            FormulaRole::Lemma => write!(f,"Lemma"),
            FormulaRole::Theorem => write!(f,"Theorem"),
            FormulaRole::Corollary => write!(f,"Corollary"),
            FormulaRole::Conjecture => write!(f,"Conjecture"),
            FormulaRole::Negated_conjecture => write!(f,"Negated_conjecture"),
            FormulaRole::Plain => write!(f,"Plain"),
            FormulaRole::Type => write!(f,"Type"),
            FormulaRole::Fi_domain => write!(f,"Fi_domain"),
            FormulaRole::Fi_functors => write!(f,"Fi_functors"),
            FormulaRole::Fi_predicates => write!(f,"Fi_predicates"),
            FormulaRole::Unknown => write!(f,"Unknown"),
        }
    }
}

impl fmt::Display for Term {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self{
            Term::Variable(ref s) => write!(f,"{}", s),
            Term::Constant(ref s) => write!(f,"{}", s),
            Term::Functor(ref s, ref args) => {
                write!(f,"{}",s);
                write!(f,"(");
                for ii in 0..args.len(){
                    write!(f,"{}",args[ii]);
                    if ii < args.len()-1{
                        write!(f,", ");
                    }
                }
                write!(f,")")
            }
        }
    }
}


impl Term{
    pub fn to_str(&self, st:&Symtable) -> String{
        match *self{
            Term::Variable(ref s) => {
                st.get(*s)
            },
            Term::Constant(ref s) => st.get(*s),
            Term::Functor(ref s, ref args) => {
                let mut res = "".to_string();
                res.push_str(&st.get(*s));
                res.push_str("(");
                for ii in 0..args.len(){
                    res.push_str(&args[ii].to_str(st));
                    if ii < args.len()-1{
                        res.push_str(", ");
                    }
                }
                res.push_str(")");
                res
            }
        }
    }
}

impl fmt::Display for GTerm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self{
            GTerm::Variable(ref s) => write!(f,"{}", s),
            GTerm::Constant(ref s) => write!(f,"{}", s),
            GTerm::Functor(ref s, ref args) => {
                write!(f,"{}",s);
                write!(f,"(");
                for ii in 0..args.len(){
                    write!(f,"{}",args[ii]);
                    if ii < args.len()-1{
                        write!(f,", ");
                    }
                }
                write!(f,")")
            }
        }
    }
}

impl fmt::Display for FolFormula {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            FolFormula::Exists(ref vars,ref formula) => {
                write!(f,"Exists[");
                for v in vars{
                    write!(f,"{},",v);
                }
                write!(f,"]: ({})",formula)
            },
            FolFormula::Forall(ref vars,ref formula) => {
                write!(f,"Forall[");
                for v in vars{
                    write!(f,"{},",v);
                }
                write!(f,"]: ({})",formula)
            },
            FolFormula::Equiv(ref f1,ref f2) => {
                write!(f,"{} <=> {}",f1,f2)
            },
            FolFormula::Impl(ref f1,ref f2) => {
                write!(f,"{} => {}",f1,f2)
            },
            FolFormula::Or(ref s) => {
                write!(f,"(");
                for d in s {
                    write!(f,"{} | ",d);
                }
                write!(f,")")
            },
            FolFormula::And(ref s) => {
                write!(f,"(");
                for d in s {
                    write!(f,"{} & ",d);
                }
                write!(f,")")
            },
            FolFormula::Neg(ref formula) => write!(f,"~({})",formula),
            FolFormula::Atom(ref t) => write!(f,"{}",t),
        }
    }
}
// impl fmt::Display for AnnotatedFormula{
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
//         match *self{
//             AnnotatedFormula::Fof((_,_,ref formula)) => write!(f,"{}",formula),
//         }
//     }
// }

//=========================================
//Тут хранятся все символы. id -> string
pub struct Symtable{
    //текщий свободный символ
    pub currIndex: usize,
    //Чтоб можно было по айдишнику символа восстановить его строковый вариант для вывода на экран
    pub smap: HashMap<usize,String>,
}

impl Symtable{
    pub fn new() -> Symtable{
        Symtable{currIndex: 1, smap: HashMap::<usize,String>::new()}
    }

    pub fn get_next_index(&mut self) -> usize{
        let i = self.currIndex;
        self.currIndex = self.currIndex + 1;
        i
    }
    pub fn add_binding(&mut self, i:usize,s:String){
        self.smap.insert(i,s);
    }

    pub fn print(&self){
        println!("Symtable: {:?}",self.smap);
    }

    pub fn get(&self, i:usize) -> String{
        self.smap.get(&i).unwrap().clone()
    }
}

//Эта структура чисто для переменных
//varmap - самое свежее связывание переменных
pub struct VarSymBindingStack<'a>{
    varmap: HashMap<String,usize>,
    tail: Option<Box<&'a VarSymBindingStack<'a>>>,
}

impl <'a>VarSymBindingStack<'a>{
    //bs - предыдущие связывания переменных
    //vars - новые переменные, для них создаем айдишники, добавляем в таблицу символов
    //и добавляем к стеку, эти наименования в большем приоритете чем предыдущие
    pub fn new(bs: Option<Box<&'a VarSymBindingStack<'a>>>, vars: &Vec<GTerm>, st: &mut Symtable) -> VarSymBindingStack<'a>{
        let mut vm = HashMap::<String,usize>::new();
        for v in vars{
            match *v{
                GTerm::Variable(ref s) => {
                    let vi = st.get_next_index();
                    vm.insert(s.clone(),vi);
                    st.add_binding(vi,s.clone());
                },
                _ => panic!("В кванторе вместо переменных что-то другое."),
            }
        }
        VarSymBindingStack{varmap:vm, tail:bs}
    }
    pub fn get_id(&self, s:String)->usize{
        if let Some(x) = self.varmap.get(&s){
            *x
        }else {
            match self.tail{
                Some(ref t) => {
                    (*t).get_id(s)
                },
                None => panic!("Переменная не связана никаким квантором."),
            }
        }
    }

    pub fn print(&self){
        println!("{:?}",self.varmap);
        if let Some(ref x) = self.tail {
            (*x).print();
        }
    }
}

//Временная структура для хранения связок String -> id.
//Используется только на этапе трансляци, чтоб одинаковым символам одинаковые айди назначались
//Смысл как и у VarSymBindingStack, только не нужен огород из стека.
pub struct SymBinding{
    smap: HashMap<String,usize>,
}

impl SymBinding{
    pub fn new() -> SymBinding{
        SymBinding{smap:HashMap::<String,usize>::new()}
    }

    pub fn get_id(&mut self, s:String, st:&mut Symtable) -> usize {
        //NOTE: Govnokod!
        let mut b = false;
        {
            let sid = self.smap.get(&s);
            b = match sid{
                Some(_) => true,
                None => false,
            };
        }
        if b {
            *(self.smap.get(&s).unwrap())
        }else{
            let vi = st.get_next_index();
            self.smap.insert(s.clone(),vi);
            st.add_binding(vi,s.clone());
            vi
        }
    }

    pub fn print(&self){
        println!("SymBinding: {:?}",self.smap);
    }
}

pub fn create_term(gt:GTerm,vsbs:&VarSymBindingStack, st:&mut Symtable, sb:&mut SymBinding) -> Term{
    match gt{
        GTerm::Variable(s) => {
            Term::Variable(vsbs.get_id(s))
        },
        GTerm::Constant(s) => {
            let i = sb.get_id(s,st);
            Term::Constant(i)
        },
        GTerm::Functor(s, args) => {
            let i = sb.get_id(s,st);
            let v = create_vec_terms(args, vsbs, st, sb);
            Term::Functor(i,v)
        },
    }
}

pub fn create_vec_terms(v:Vec<GTerm>, vsbs:&VarSymBindingStack, st:&mut Symtable, sb:&mut SymBinding) -> Vec<Term>{
    v.into_iter().map(|t| create_term(t,vsbs,st,sb)).collect()
}
