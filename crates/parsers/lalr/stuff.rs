use def::*;
use parser::*;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;


extern crate regex;

fn is_double_quote(c: char) -> bool {
    c as u8 == 34
}

//Удаление комментариев. Свою кривую функцию заменил на чужую кривую.
//NOTE: Govnokod!
fn prep(s: &str) -> String {
    // First remove all comment blocks.
    let comment_block_regex = regex::Regex::new(r"[/][*]([^*]*[*][*]*[^/*])*[^*]*[*][*]*[/]").expect("This should always work");
    let s2 = comment_block_regex.replace_all(s, regex::NoExpand(""));
    let mut s3 = "".to_owned();

    // Then remove all comment lines.
    // This is a bit tricky due to the possibility of single-quoted and double-quoted strings.
    // Escaping is also annoying.
    // All in all this part most likely has some subtle bugs.
    for l in s2.lines() {
        let mut inside_single_quoted = false;
        let mut inside_double_quoted = false;
        let mut comment_start_location = None;
        let mut escaping_next = false;

        for (i, c) in l.chars().enumerate() {
            assert!(!inside_single_quoted || !inside_double_quoted);

            if (inside_single_quoted || inside_double_quoted) && c == '\\' {
                // Two slashes in a row is just a character, otherwise raise escape flag.
                escaping_next = !escaping_next;
                continue;
            } else if !inside_double_quoted && c == '\'' {
                if !escaping_next {
                    inside_single_quoted = !inside_single_quoted;
                }
            } else if !inside_single_quoted && is_double_quote(c)  {
                if !escaping_next {
                    inside_double_quoted = !inside_double_quoted;
                }
            } else if c == '%' && !inside_single_quoted && !inside_double_quoted{
                // This percentage sign wasn't inside a string, so it is a real comment.
                comment_start_location = Some(i);
                break;
            }

            escaping_next = false;
        }

        // Did we find a comment? If so remove it.
        if let Some(pos) = comment_start_location {
            s3.push_str(&l[0..pos]);
        } else {
            s3.push_str(l);
        }
    }

    s3
}

//=================================================
pub fn parse_tptp_file(filename: &str) -> Vec<AnnotatedFolFormula> {
    //открываем файл
    let path = Path::new(filename);
    let display = path.display();
    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, Error::description(&why)),
        Ok(file) => file,
    };
    let mut s = String::new();// <- сюда записываем содержимое файла
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display, Error::description(&why)),
        Ok(_) => println!("{}: [Ok]", display),
    }
    //удаляем комментарии
    let ncs = prep(&s);
    //парсим
    let tptpinputvec = parse_TPTP_file(&ncs);
    match tptpinputvec{
        Ok(v) => {
            v
        },
        Err(_) => panic!("parse_tptp_file error")
    }

}

pub fn parse_tptp_problem(s: &str) -> Problem {
    let p = Problem{ff:parse_tptp_file(s)};
    p
}
