extern crate lalrpop;

use std::process::Command;
// use std::path::Path;

fn main() {
    lalrpop::process_root().unwrap();
    Command::new("/usr/bin/bash")
        .args(&["parser.sh"])
        .status().unwrap();
}
