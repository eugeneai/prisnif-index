extern crate libc;

use self::libc::{c_char, int32_t};
use std::ffi::CStr;
use std::str;
use std::ffi::CString;
use std::fmt;

pub type Args=Vec<Box<Tree>>;

#[no_mangle]
pub enum Tree {
    None,
    Token(&'static str, &'static str),
    Node(&'static str, Args)
}

impl fmt::Display for Tree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Tree::None => {write!(f,"none")},
            Tree::Token(t, i) => {write!(f,"{}[{}]",t,i)},
            Tree::Node(n, ref args) => {
                try!(write!(f,"{}/{}(",n,args.len()));
                for (count, a) in args.iter().enumerate() {
                    if count != 0 { try!(write!(f, ", ")); }
                    try!(write!(f, "{}", a));
                }
                write!(f, ")")
            }
        }
    }
}

fn token_proc(tok: *const c_char)->Option<&'static str> {
    if tok.is_null() {
        None
    } else {
        let c_str = unsafe {
            CStr::from_ptr(tok)
        };
        let r_str = str::from_utf8(c_str.to_bytes()).unwrap();
        Some(r_str)
    }
}

#[no_mangle]
pub extern fn p_token(tok: *const c_char, sym_idx: int32_t) -> Box<Tree> {
    let tok = token_proc(tok);
    let answer:Tree = match tok {
        None => Tree::None,
        Some(s) => {
            let sym = token_proc(unsafe {get_lval(sym_idx)}).unwrap();
            Tree::Token(s, sym)
        }
    };
    // print_tree_node(&answer);
    Box::new(answer)
}

#[no_mangle]
pub extern fn p_accept(tree: Box<Tree>) {
    let t = *tree;
    println!(">>>>>> Printing >>>>>>>");
    println!("{}", t);
}

pub fn tree_release(t: Box<Tree>) {
    match *t {
        Tree::Node(_, args) => {
            for a in args {
                tree_release(a);
            }
        },
        _ => {}
    }
}

fn add_arg(vec: &mut Args, a: Box<Tree>) {
    let pa:*const Tree = &*a;
    if ! pa.is_null() {
        vec.push(a);
    } else {
        unsafe {
            fictitious_drop(a);
        };
    }
}

#[no_mangle]
pub extern fn p_build_tree(sym: *const c_char,
                     a: Box<Tree>,
                     b: Box<Tree>,
                     c: Box<Tree>,
                     d: Box<Tree>,
                     e: Box<Tree>,
                     f: Box<Tree>,
                     g: Box<Tree>,
                     h: Box<Tree>,
                     i: Box<Tree>,
                     j: Box<Tree>) -> Box<Tree> {
    let sym = token_proc(sym);
    let answer:Tree = match sym {
        None =>
            Tree::None,
        Some(s) => {
            let mut args = Vec::new();
            add_arg(&mut args, a);
            add_arg(&mut args, b);
            add_arg(&mut args, c);
            add_arg(&mut args, d);
            add_arg(&mut args, e);
            add_arg(&mut args, f);
            add_arg(&mut args, g);
            add_arg(&mut args, h);
            add_arg(&mut args, i);
            add_arg(&mut args, j);
            Tree::Node(s, args)
         }
    };
    Box::new(answer)
}

#[link(name = "fbtptpparser")]
#[allow(improper_ctypes)]
extern {
    fn parse() -> i32;
    fn parse_file0(file_name: *const c_char) -> i32;
    fn fictitious_drop(a:Box<Tree>);
    fn get_lval(idx:int32_t)-> *const c_char;
}

#[test]
fn test_parser_from_file() {
    parse_file("KRS064+1.p");
}

pub fn parse_file(file_name: &str) -> i32 {
    let filnam = CString::new(file_name).unwrap();
    unsafe {
        parse_file0(filnam.as_ptr())
    }
}

fn parse_input(file_name: &str) -> i32 {
    parse_file(file_name)
}

fn parse_stdin() -> i32 {
    unsafe {
        parse()
    }
}
