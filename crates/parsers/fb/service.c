#include "stdio.h"
#include <stdarg.h>
#include <unistd.h>
#include <stdint.h>

void yyerror(char *s, ...)
{
    va_list ap;
    va_start(ap, s);

    fprintf(stderr, "error: ");
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");
    exit(1);
}

int yywrap() {
    return 1;
}

int32_t parse() {
  int32_t rc=0;
  rc=yyparse();
  return rc;
}

int32_t parse_file0(char * name) {
  int32_t rc;
  extern FILE *yyin;
  yyin = fopen(name, "r");
  rc = yyparse();
  return rc;
};

void fictitious_drop(void * ptr) {
};

extern char* tptp_lval[];
char * get_lval(int32_t idx) {
  return tptp_lval[idx];
};

__main( int argc, char *argv[] )
{
    int rc=0;
    /*
    extern FILE *yyin;
    ++argv;--argc;
    yyin = fopen( argv[0], "r");
    //yydebug = 1;
    //errors = 0;
    */
    rc=yyparse ();
    //fclose(yyin);
    return rc;
}
