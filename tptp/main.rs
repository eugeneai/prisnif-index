extern crate getopts;
extern crate lalr;
extern crate fb;
extern crate def;
extern crate trans;

use getopts::Options;
use std::env;

extern crate time;
use self::time::PreciseTime;

use self::trans::*;

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} FILE [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() {
    // test
    
    fn test_name() {
        
    }

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("h", "help", "print this help page");
    opts.optflag("p", "problem", "output problems to jsfile");
    opts.optflag("j", "fof", "output fof to jsfile");
    opts.optflag("l", "lalr", "use LALRpop parser");
    opts.optflag("b", "bison", "use flex/yacc parser");
    opts.optflagopt("f", "input", "ison-parsing from a file argument", "FILE.p");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m },
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    let input:String = match matches.opt_str("f") {
        Some(x) => x,
        None => "problems/test.p".to_string()
    };
    let input:String = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else {
        input
    };

    println!("Starting...");

    // "problems/test.p"

    let start = PreciseTime::now();

    let tptp_problem = if matches.opt_present("l") {
        lalr::parse_tptp_problem(&input)
    } else if matches.opt_present("b") {
        // fb::parse_input(&input) // FIXME uint32
        lalr::parse_tptp_problem(&input) // STUB
    } else {
        panic!("Either LALRpop (-l) or flex/bison parser must be supplied as argument.")
    };

    let end = PreciseTime::now();
    println!("TPTP Parsing: {} seconds.", start.to(end));

    if matches.opt_present("p") {
        println!("Creating JS for the problem...");
        problem_to_jsfile(&tptp_problem);
    };

    println!("Converting the problem to FOF...");
    let fof = problem_to_fof(tptp_problem);

    if matches.opt_present("j") {
        println!("Creating JS for its FOF...");
        fof_to_jsfile(&fof);
    }

    println!("Converting FOF to PCF...");

    let pcf = fof_to_pcf(fof);

    println!("...Finish");

}
